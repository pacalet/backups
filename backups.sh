#!/bin/bash

# backups - This file is part of backups
# Copyright (C) - Renaud Pacalet
# Contacts: renaud.pacalet@free.fr
#
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file COPYING, which
# you should have received as part of this distribution. The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt

# Customization is done through the configuration file
# (/usr/local/etc/backups.conf by default). See README.md for details. Modifying
# this file should not be necessary.

# Log functions
function LOG () {
	echo "$me: $*" >> "$tmplog"
	if (( BACKUPS_DEBUG )); then
		echo "$me: $*"
	fi
}

function DBG () {
	echo "$me: DEBUG: $*" >> "$tmplog"
	if (( BACKUPS_DEBUG )); then
		echo "$me: DEBUG: $*"
	fi
}

function ERR () {
	echo "$me: ERROR: $*" >> "$tmplog"
	if (( BACKUPS_DEBUG )); then
		echo "$me: ERROR: $*"
	fi
	exit 1
}

# Helper functions
# Print conviguration variables
function PRINTDEBUG () {
	LOG "*********************************************"
	LOG "* DEBUG: SUMMARY OF CONFIGURATION VARIABLES *"
	LOG "*********************************************"
	LOG "	BACKUPS_PID_FILE	$BACKUPS_PID_FILE"
	LOG "	BACKUPS_TMP_DIR		$BACKUPS_TMP_DIR"
	LOG "	BACKUPS_LOG_FILE	$BACKUPS_LOG_FILE"
	if [[ -z "$BACKUPS_EMAIL" ]]; then
		str="undefined (no e-mail notifications)"
	else
		str=""
	fi
	LOG "	BACKUPS_EMAIL		$BACKUPS_EMAIL$str"
	if (( BACKUPS_SIZE_MAX == 0 )); then
		str=" (no limit)"
	else
		str=""
	fi
	LOG "	BACKUPS_SIZE_MAX	$BACKUPS_SIZE_MAX$str"
	if (( BACKUPS_YEARLY == 0 )); then
		str=" (no limit)"
	else
		str=""
	fi
	LOG "	BACKUPS_YEARLY		$BACKUPS_YEARLY$str"
	if (( BACKUPS_MONTHLY == 0 )); then
		str=" (no limit)"
	else
		str=""
	fi
	LOG "	BACKUPS_MONTHLY		$BACKUPS_MONTHLY$str"
	if (( BACKUPS_DAILY == 0 )); then
		str=" (no limit)"
	else
		str=""
	fi
	LOG "	BACKUPS_DAILY		$BACKUPS_DAILY$str"
	if (( BACKUPS_MIN_INTERVAL == 0 )); then
		str=" (no limit)"
	else
		str=""
	fi
	LOG "	BACKUPS_MIN_INTERVAL	$BACKUPS_MIN_INTERVAL$str"
	if [[ -z "$BACKUPS_FILTER" ]]; then
		str="undefined (no global filter rules file)"
	else
		str=""
	fi
	LOG "	BACKUPS_FILTER		$BACKUPS_FILTER$str"
	if [[ -z "$BACKUPS_OPTIONS" ]]; then
		str="undefined (no extra rsync options)"
	else
		str=""
	fi
	LOG "	BACKUPS_OPTIONS		$BACKUPS_OPTIONS$str"
	if (( isremote )); then
		str=" (remote at $BACKUPS_SSH_USER_AT_HOST)"
	else
		str=" (local)"
	fi
	LOG "	BACKUPS_STORE		$BACKUPS_STORE$str"
	LOG "	BACKUPS_SNAPSHOTS_DIR	$BACKUPS_SNAPSHOTS_DIR"
	LOG "	BACKUPS_LOGS_DIR	$BACKUPS_LOGS_DIR"
	LOG "	BACKUPS_SOURCE		$BACKUPS_SOURCE"
	LOG "*********************************************"
	LOG
}

# Translate a YYYYMMDDhhmmss date into timestamp (seconds since 1970-01-01
# 00:00:00 UTC). Usage: get_timestamp date
function get_timestamp () {
	[[ "$1" =~ (....)(..)(..)(..)(..)(..) ]]
	d="${BASH_REMATCH[1]}-${BASH_REMATCH[2]}-${BASH_REMATCH[3]} ${BASH_REMATCH[4]}:${BASH_REMATCH[5]}:${BASH_REMATCH[6]}"
	date --date="$d" +"%s"
}

# Extract the year from a YYYYMMDDhhmmss date (e.g. 2013). Usage: get_year date
function get_year () {
	printf "${1:0:4}"
}

# Extract the month from a YYYYMMDDhhmmss date (01-12). Usage: get_month date
function get_month () {
	local ret="${1:4:2}"
	printf "${ret#0}"
}

function RSYNC () {
	if (( BACKUPS_DEBUG )); then
		BACKUPS_OPTIONS="--dry-run $BACKUPS_OPTIONS"
	fi
	if (( $# == 2 )); then
		if [[ -z "$BACKUPS_FILTER" ]]; then
			LOG "rsync $BACKUPS_OPTIONS --link-dest=\"$1\" $BACKUPS_SOURCE \"$2\""
			rsync $BACKUPS_OPTIONS --link-dest="$1" $BACKUPS_SOURCE "$2" >> "$tmplog" 2>&1
		else
			LOG "rsync $BACKUPS_OPTIONS --filter=\". $BACKUPS_FILTER\" --link-dest=\"$1\" $BACKUPS_SOURCE \"$2\""
			rsync $BACKUPS_OPTIONS --filter=". $BACKUPS_FILTER" --link-dest="$1" $BACKUPS_SOURCE "$2" >> "$tmplog" 2>&1
		fi
	elif (( $# == 1 )); then
		if [[ -z "$BACKUPS_FILTER" ]]; then
			LOG "rsync $BACKUPS_OPTIONS $BACKUPS_SOURCE \"$1\""
			rsync $BACKUPS_OPTIONS $BACKUPS_SOURCE "$1" >> "$tmplog" 2>&1
		else
			LOG "rsync $BACKUPS_OPTIONS --filter=\". $BACKUPS_FILTER\" $BACKUPS_SOURCE \"$1\""
			rsync $BACKUPS_OPTIONS --filter=". $BACKUPS_FILTER" $BACKUPS_SOURCE "$1" >> "$tmplog" 2>&1
		fi
	else
		ERR "RSYNC $*: invalid number of arguments. Exiting..."
	fi
}

function BZIP2 () {
	if (( $# == 2 )); then
		$EVAL "bzip2 -9c \"$1\" > \"$2\""
	elif (( $# == 1 )); then
		$EVAL "bzip2 -9 \"$1\""
	else
		ERR "BZIP2 $*: invalid number of arguments. Exiting..."
	fi
}
function CATLOG () {
	if (( $# == 2 )); then
		$EVAL "cat \"$1\" >> \"$2\""
	else
		ERR "CATLOG $*: invalid number of arguments. Exiting..."
	fi
}
function CLEAN () {
	if (( $# >= 1 )); then
		$EVAL "echo \"rm -rf $*\" >> \"$tmpclean\""
	else
		ERR "CLEAN: invalid number of arguments. Exiting..."
	fi
}
function CP () {
	if (( $# >= 2 )); then
		if (( isremote )); then
			$EVAL "scp -q $*"
		else
			$EVAL "cp $*"
		fi
	else
		ERR "CP $*: invalid number of arguments. Exiting..."
	fi
}
function MAIL () {
	if (( $# == 2 )); then
		if [[ -n "$BACKUPS_EMAIL" ]]; then
			LOG "mail -s \"$1\" $BACKUPS_EMAIL < \"$2\""
			$EVAL "mail -s \"$1\" $BACKUPS_EMAIL < \"$2\""
		else
			LOG "BACKUPS_EMAIL variable undefined. Skipping e-mail notification..."
		fi
	else
		ERR "MAIL $*: invalid number of arguments. Exiting..."
	fi
}
function RECORDPID () {
	if (( $# == 2 )); then
		$EVAL "echo \"$1\" > \"$2\""
	else
		ERR "RECORDPID $*: invalid number of arguments. Exiting..."
	fi
}
function RM () {
	if (( $# >= 1 )); then
		if (( isremote )); then
			$EVAL "ssh \"$BACKUPS_SSH_USER_AT_HOST\" \"rm -rf $*\""
		else
			$EVAL "rm -rf $*"
		fi
	else
		ERR "RM: invalid number of arguments. Exiting..."
	fi
}
function SSH () {
	if (( $# == 2 )); then
		$EVAL "ssh \"$1\" \"$2\""
	else
		ERR "SSH $*: invalid number of arguments. Exiting..."
	fi
}
		
# Main function: calls rsync to create a snapshot in BACKUPS_STORE/snapshots.
# Usage: backup
function backup () {

	# Debug or run
	if (( BACKUPS_DEBUG )); then
		EVAL=DBG
		PRINTDEBUG
	else
		EVAL=eval
	fi

	# Check mandatory configuration variables
	if [[ -z "$BACKUPS_STORE" ]]; then
		ERR "$me: ERROR: BACKUPS_STORE variable undefined. Exiting..."
	fi
	if [[ -z "$BACKUPS_SOURCE" ]]; then
		ERR "$me: ERROR: BACKUPS_SOURCE variable undefined. Exiting..."
	fi
	
	# Actual values of configuration variables
	BACKUPS_PID_FILE="${BACKUPS_PID_FILE:=$BACKUPS_PID_FILE_DEFAULT}"
	BACKUPS_TMP_DIR="${BACKUPS_TMP_DIR:=$BACKUPS_TMP_DIR_DEFAULT}"
	BACKUPS_LOG_FILE="${BACKUPS_LOG_FILE:=$BACKUPS_LOG_FILE_DEFAULT}"
	BACKUPS_SIZE_MAX="${BACKUPS_SIZE_MAX:=$BACKUPS_SIZE_MAX_DEFAULT}"
	BACKUPS_YEARLY="${BACKUPS_YEARLY:=$BACKUPS_YEARLY_DEFAULT}"
	BACKUPS_MONTHLY="${BACKUPS_MONTHLY:=$BACKUPS_MONTHLY_DEFAULT}"
	BACKUPS_DAILY="${BACKUPS_DAILY:=$BACKUPS_DAILY_DEFAULT}"
	BACKUPS_MIN_INTERVAL="${BACKUPS_MIN_INTERVAL:=$BACKUPS_MIN_INTERVAL_DEFAULT}"
	BACKUPS_MAX_NUMBER_OF_SNAPSHOTS_TO_DELETE="${BACKUPS_MAX_NUMBER_OF_SNAPSHOTS_TO_DELETE:=$BACKUPS_MAX_NUMBER_OF_SNAPSHOTS_TO_DELETE_DEFAULT}"
	BACKUPS_MAX_NUMBER_OF_LOGS_TO_DELETE="${BACKUPS_MAX_NUMBER_OF_LOGS_TO_DELETE:=$BACKUPS_MAX_NUMBER_OF_LOGS_TO_DELETE_DEFAULT}"
	BACKUPS_CREATE_FULL="${BACKUPS_CREATE_FULL:=$BACKUPS_CREATE_FULL_DEFAULT}"
	
	# Computed variables
	tmplog="$BACKUPS_TMP_DIR/$current"            # Temporary log file
	shorttmplog="$BACKUPS_TMP_DIR/$current.log"   # Short temporary log file
	tmpclean="$BACKUPS_TMP_DIR/$current.clean.sh" # Temporary clean file
	snapshot="$current"                           # Current snapshot
	log="$current.bz2"                            # Current log file
	clean="$current.clean.sh"                     # Current clean file
	if [[ "$BACKUPS_STORE" = *:* ]]; then
		isremote=1
		BACKUPS_SSH_USER_AT_HOST="${BACKUPS_STORE%%:*}"
		BACKUPS_STORE="${BACKUPS_STORE##*:}"
		BACKUPS_STORE=$(ssh "$BACKUPS_SSH_USER_AT_HOST" "cd $BACKUPS_STORE; pwd -P")
		prefix="$BACKUPS_SSH_USER_AT_HOST:"
	else
		BACKUPS_STORE=$(cd $BACKUPS_STORE; pwd -P)
	fi
	BACKUPS_SNAPSHOTS_DIR="$BACKUPS_STORE"/snapshots
	BACKUPS_LOGS_DIR="$BACKUPS_STORE"/logs
	BACKUPS_EMAIL_SUBJECT="|"

	# Create BACKUPS_TMP_DIR if it does not exist
	mkdir -p "$BACKUPS_TMP_DIR"

	LOG "started"

	# Local variables
	local now=$(get_timestamp "$current")
	local newest=""
	local y=0
	local m=0
	local -A snapshots logs
	local ns=0
	local cmds cmdl s l i tmp age ny nm size dc dn

	# First make sure we're running as root
	if (( BACKUPS_ROOT_ONLY )); then
		if (( UID != 0 )); then
			LOG "sorry, must be root. Exiting..."
			BACKUPS_EMAIL_SUBJECT="$BACKUPS_EMAIL_SUBJECT NOT ROOT |"
			return 1
		fi
	fi

	# If a backup operation is already running or previous backup crashed.
	if [[ -r $BACKUPS_PID_FILE ]]; then
		LOG "another backup already running or crashed (PID=$pid). Exiting..."
		BACKUPS_EMAIL_SUBJECT="$BACKUPS_EMAIL_SUBJECT ALREADY RUNNING |"
		return 1
	fi
	RECORDPID "$$" "$BACKUPS_PID_FILE"

	# Detect non-normally terminated or too old snapshots for removal. Find most recent, normally terminated, snapshot for hard-linked reference.
	if (( isremote )); then
		cmds="ssh "$BACKUPS_SSH_USER_AT_HOST" \"mkdir -p $BACKUPS_SNAPSHOTS_DIR; cd $BACKUPS_SNAPSHOTS_DIR; for s in *; do echo \\\$s; done\""
		cmdl="ssh "$BACKUPS_SSH_USER_AT_HOST" \"mkdir -p $BACKUPS_LOGS_DIR; cd $BACKUPS_LOGS_DIR; for l in *; do echo \\\$l; done\""
	else
		cmds="mkdir -p $BACKUPS_SNAPSHOTS_DIR; cd $BACKUPS_SNAPSHOTS_DIR; for s in *; do echo \$s; done"
		cmdl="mkdir -p $BACKUPS_LOGS_DIR; cd $BACKUPS_LOGS_DIR; for l in *; do echo \$l; done"
	fi
	while read s; do
		if [[ -z "$s" || "$s" == "*" || "$s" == "current" ]]; then
			continue;
		fi
		snapshots[$ns]="$s"
		ns=$(( ns + 1 ))
	done < <(eval $cmds)
	while read l; do
		if [[ -z "$l" || "$l" == "*" || "$l" == "current" ]]; then
			continue;
		fi
		l=$(basename "$l" .bz2)
		logs["$l"]=1
	done < <(eval $cmdl)
	std="" # Snapshots to delete
	nstd=0 # Number of snapshots to delete
	ltd="" # Logs to delete
	nltd=0 # Number of logs to delete
	for (( i = 0; i < ns; i += 1 )); do
		s=${snapshots[$i]}
		if [[ -z "${logs[$s]}" ]]; then
			LOG "$s snapshot did not terminate normally. Will not be used as reference."
			std="$std $BACKUPS_SNAPSHOTS_DIR/$s"
			(( nstd += 1 ))
			continue
		fi
		tmp=$(get_timestamp "$s")
		age=$(( ( now - tmp ) / ( 24 * 3600 ) ))
		ny=$(get_year "$s")
		nm=$(get_month "$s")
		if (( BACKUPS_YEARLY != 0 && ny != y && age > BACKUPS_YEARLY )); then # First snapshot of year and too old => delete
			LOG "$s snapshot (year's first) older than $BACKUPS_YEARLY days. Will not be used as reference."
			std="$std $BACKUPS_SNAPSHOTS_DIR/$s"
			(( nstd += 1 ))
			ltd="$ltd $BACKUPS_LOGS_DIR/$s.bz2"
			(( nltd += 1 ))
		elif (( ny != y )); then # First snapshot of year and not too old => keep
			y="$ny"
			m="$nm"
			newest="$s"
		elif (( BACKUPS_MONTHLY != 0 && nm != m && age > BACKUPS_MONTHLY )); then # First snapshot of month and too old => delete
			LOG "$s snapshot (month's first) older than $BACKUPS_MONTHLY days. Will not be used as reference."
			std="$std $BACKUPS_SNAPSHOTS_DIR/$s"
			(( nstd += 1 ))
			ltd="$ltd $BACKUPS_LOGS_DIR/$s.bz2"
			(( nltd += 1 ))
			m=0 # Next snapshot is first of its month
		elif (( nm != m )); then # First snapshot of month and not too old => keep
			m="$nm"
			newest="$s"
		elif (( BACKUPS_DAILY != 0 && age > BACKUPS_DAILY )); then
			LOG "$s snapshot older than $BACKUPS_DAILY days. Will not be used as reference."
			std="$std $BACKUPS_SNAPSHOTS_DIR/$s"
			(( nstd += 1 ))
			ltd="$ltd $BACKUPS_LOGS_DIR/$s.bz2"
			(( nltd += 1 ))
		else # Not too old => keep
			newest="$s"
		fi
	done
	for l in ${!logs[*]}; do
		if [[ ! "${snapshots[*]}" = *$l* ]]; then
			LOG "Orphan $l.bz2 log file."
			ltd="$ltd $BACKUPS_LOGS_DIR/$l.bz2"
			(( nltd += 1 ))
		fi
	done
    
	# Delete obsolete snapshots and logs
	if (( nstd > 0 && nstd <= BACKUPS_MAX_NUMBER_OF_SNAPSHOTS_TO_DELETE )); then
		LOG "Deleting snapshots $std."
		BACKUPS_EMAIL_SUBJECT="$BACKUPS_EMAIL_SUBJECT SDEL:$nstd |"
		RM $std
	elif (( nstd > BACKUPS_MAX_NUMBER_OF_SNAPSHOTS_TO_DELETE )); then
		LOG "Adding $std to the $BACKUPS_STORE/$clean cleanup script."
		BACKUPS_EMAIL_SUBJECT="$BACKUPS_EMAIL_SUBJECT SDEL:SCRIPT |"
		CLEAN $std
	fi
	if (( nltd > 0 && nltd <= BACKUPS_MAX_NUMBER_OF_LOGS_TO_DELETE )); then
		LOG "Deleting logs $ltd."
		BACKUPS_EMAIL_SUBJECT="$BACKUPS_EMAIL_SUBJECT LDEL:$nstd |"
		RM $ltd
	elif (( nltd > BACKUPS_MAX_NUMBER_OF_LOGS_TO_DELETE )); then
		LOG "Adding $ltd to the $BACKUPS_STORE/$clean cleanup script."
		BACKUPS_EMAIL_SUBJECT="$BACKUPS_EMAIL_SUBJECT LDEL:SCRIPT |"
		CLEAN $ltd
	fi
	# Check disk quota
	if (( isremote )); then
		size=$(ssh "$BACKUPS_SSH_USER_AT_HOST" "df -kP $BACKUPS_STORE | awk 'END {print \$3}'")
	else
		size=$(df -kP "$BACKUPS_STORE" | awk 'END {print $3}')
	fi
	size=$(( size / ( 1024 ** 2 ) ))
	if (( BACKUPS_SIZE_MAX != 0 && size > BACKUPS_SIZE_MAX )); then
		LOG "disk quota exceeded ($size > $BACKUPS_SIZE_MAX). Exiting..."
		BACKUPS_EMAIL_SUBJECT="$BACKUPS_EMAIL_SUBJECT QUOTA EXCEEDED |"
		return 1
	elif (( BACKUPS_SIZE_MAX == 0 )); then
		LOG "disk quota OK (no limit)"
		BACKUPS_EMAIL_SUBJECT="$BACKUPS_EMAIL_SUBJECT DISK:$size/INF |"
	else
		LOG "disk quota OK ($size <= $BACKUPS_SIZE_MAX)"
		BACKUPS_EMAIL_SUBJECT="$BACKUPS_EMAIL_SUBJECT DISK:$size/$BACKUPS_SIZE_MAX |"
	fi

	if [[ -n "$newest" ]]; then
		dc=$(get_timestamp "$current")
		dn=$(get_timestamp "$newest")
		if (( BACKUPS_MIN_INTERVAL == 0 || dc - dn > BACKUPS_MIN_INTERVAL )); then
			LOG "initializing new snapshot: $snapshot"
			BACKUPS_EMAIL_SUBJECT="$BACKUPS_EMAIL_SUBJECT NEW INC |"
		else
			LOG "last snapshot recent enough ($BACKUPS_SNAPSHOTS_DIR/$newest). Exiting..."
			BACKUPS_EMAIL_SUBJECT="$BACKUPS_EMAIL_SUBJECT RECENT ENOUGH |"
			return 1
		fi
	elif (( BACKUPS_CREATE_FULL == 0 )); then
		LOG "creating full snapshots disabled, exiting (turn BACKUPS_CREATE_FULL option on if you really want to create a full snapshot)"
		BACKUPS_EMAIL_SUBJECT="$BACKUPS_EMAIL_SUBJECT FULL DISABLED |"
		return 1
	else
		LOG "initializing full snapshot: $snapshot"
		BACKUPS_EMAIL_SUBJECT="$BACKUPS_EMAIL_SUBJECT NEW FULL |"
	fi
  
	# Backup source in snapshot
	LOG "synchronization started at $(date +'%F %T')"
	if [[ -n "$newest" ]]; then
		RSYNC "$BACKUPS_SNAPSHOTS_DIR/$newest" "$prefix$BACKUPS_SNAPSHOTS_DIR/$snapshot"
	else
		RSYNC "$prefix$BACKUPS_SNAPSHOTS_DIR/$snapshot"
	fi
	if (( isremote )); then
		LOG "ssh \"$BACKUPS_SSH_USER_AT_HOST\" \"chmod a+rx $BACKUPS_SNAPSHOTS_DIR/$snapshot\""
		ssh "$BACKUPS_SSH_USER_AT_HOST" "chmod a+rx $BACKUPS_SNAPSHOTS_DIR/$snapshot"
		LOG "ssh \"$BACKUPS_SSH_USER_AT_HOST\" \"cd $BACKUPS_SNAPSHOTS_DIR; rm current; ln -s $snapshot current\""
		ssh "$BACKUPS_SSH_USER_AT_HOST" "cd $BACKUPS_SNAPSHOTS_DIR; rm current; ln -s $snapshot current"
		LOG "ssh \"$BACKUPS_SSH_USER_AT_HOST\" \"cd $BACKUPS_LOGS_DIR; rm current; ln -s $log current\""
		ssh "$BACKUPS_SSH_USER_AT_HOST" "cd $BACKUPS_LOGS_DIR; rm current; ln -s $log current"
	else
		LOG "chmod a+rx \"$BACKUPS_SNAPSHOTS_DIR/$snapshot\""
		chmod a+rx "$BACKUPS_SNAPSHOTS_DIR/$snapshot"
		LOG "rm $BACKUPS_SNAPSHOTS_DIR/current; ln -s $BACKUPS_SNAPSHOTS_DIR/$snapshot $BACKUPS_SNAPSHOTS_DIR/current"
		rm $BACKUPS_SNAPSHOTS_DIR/current; ln -s $BACKUPS_SNAPSHOTS_DIR/$snapshot $BACKUPS_SNAPSHOTS_DIR/current
		LOG "rm $BACKUPS_LOGS_DIR/current; ln -s $BACKUPS_LOGS_DIR/$log $BACKUPS_LOGS_DIR/current"
		rm $BACKUPS_LOGS_DIR/current; ln -s $BACKUPS_LOGS_DIR/$log $BACKUPS_LOGS_DIR/current
	fi
	LOG "synchronization ended at $(date +'%F %T')"
	return 0
}

################################################################################################################################################################
# Let's go...
################################################################################################################################################################

# Protect temporary files and log files
umask 077

# Default temporary log file
tmplog=/dev/null

# Test bash version (for associative arrays support)
if (( ${BASH_VERSION%%[^0-9]*} < 4 )); then
	ERR "Sorry, your version of bash (${BASH_VERSION}) does not support associative arrays. Please upgrade to version 4.0 or newer."
fi

# Default values of configuration variables
BACKUPS_PID_FILE_DEFAULT=/var/run/backups.pid
BACKUPS_TMP_DIR_DEFAULT=/tmp
BACKUPS_LOG_FILE_DEFAULT=/var/log/backups.log
BACKUPS_SIZE_MAX_DEFAULT=0
BACKUPS_YEARLY_DEFAULT=0
BACKUPS_MONTHLY_DEFAULT=0
BACKUPS_DAILY_DEFAULT=0
BACKUPS_MIN_INTERVAL_DEFAULT=0
BACKUPS_MAX_NUMBER_OF_SNAPSHOTS_TO_DELETE_DEFAULT=0
BACKUPS_MAX_NUMBER_OF_LOGS_TO_DELETE_DEFAULT=0
BACKUPS_CREATE_FULL_DEFAULT=1

# Configuration files
if (( $# == 0 )); then
	BACKUPS_CONFIGS=/usr/local/etc/backups.conf
else
	BACKUPS_CONFIGS=$*
fi

# For all configuration files
for BACKUPS_CONFIG in $BACKUPS_CONFIGS; do
	current="$(date +'%Y%m%d%H%M%S')"            # Current date (YYYYMMDDhhmmss)
	me="$0 (PID:$$ TIME:$current)"               # My name

	if [[ ! -r "$BACKUPS_CONFIG" ]]; then
		ERR "Configuration file $BACKUPS_CONFIG not found. Exiting..."
		exit 1
	fi
	. "$BACKUPS_CONFIG"

	# Backup
	backup
	status="$?"

	# Cleanup
	if [[ -f "$tmpclean" ]]; then
		if (( ! BACKUPS_DELETE_OBSOLETES )); then
			CP "$tmpclean" "$prefix$BACKUPS_STORE/$clean"
		fi
	fi

	# Notification
	LOG "rm -f \"$tmpclean\" \"$shorttmplog\" \"$BACKUPS_PID_FILE\""
	grep "^$me: " "$tmplog" > "$shorttmplog"
	CATLOG "$shorttmplog" "$BACKUPS_LOG_FILE"
	BZIP2 "$tmplog"
	if (( status )); then
		MAIL "[BACKUPS]@$HOSTNAME ($$) $BACKUPS_EMAIL_SUBJECT ABORTED |" "$shorttmplog"
	else
		MAIL "[BACKUPS]@$HOSTNAME ($$) $BACKUPS_EMAIL_SUBJECT" "$shorttmplog"
		CP "$tmplog.bz2" "$prefix$BACKUPS_LOGS_DIR/$log"
		rm -f "$tmplog.bz2"
	fi
	rm -f "$tmpclean" "$shorttmplog" "$BACKUPS_PID_FILE"
done
