LOCAL	:= local
CONF	:= $(notdir $(wildcard $(LOCAL)/backups.*))
CONFDIR	:= /usr/local/etc
BINDIR	:= /usr/local/bin
SCRIPT	:= backups.sh

all: $(BINDIR)/$(SCRIPT) $(addprefix $(CONFDIR)/,$(CONF))

$(BINDIR)/$(SCRIPT): $(SCRIPT)
ifeq ($(shell hostname),souricier)
	sed '1s=.*=#!/opt/local/bin/bash=' $< > $@
else
	cp $< $@
endif

$(addprefix $(CONFDIR)/,$(CONF)): $(CONFDIR)/%: $(LOCAL)/%
	cp $< $@
