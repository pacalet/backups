# Table of content
1. [License](#license)
1. [Content](#Content)
1. [Introduction](#Introduction)
1. [Important notes](#Notes)
   * [Safety](#Safety)
   * [Security](#Security)
   * [Extra recommendations for Mac OS X users](#MacOSX)
1. [Installation and configuration](#Installation)
1. [How does it work?](#How)
1. [Example of use](#Example)
1. [Filter rules](#Filter)
1. [Recovering files and directories](#Recovering)

# <a name="License"></a>License

    backups - This file is part of backups
    Copyright (C) - Renaud Pacalet
    Contacts: renaud.pacalet@free.fr
    
    This file must be used under the terms of the CeCILL.
    This source file is licensed as described in the file COPYING, which
    you should have received as part of this distribution. The terms
    are also available at
    http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt

# <a name="Content"></a>Content

* `COPYING`: licence
* `COPYING-FR`: license
* `README.md`: this file
* `backups.conf`: example configuration file
* `backups.filter`: example global filter rules file
* `backups.sh`: `bash` backup script

# <a name="Introduction"></a>Introduction

As its name says, `backups.sh` is a backup shell script. It uses `rsync` and has been tested on Debian, Ubuntu and Mac OS X. It should also work unmodified on most GNU/Linux distributions and should be easy to adapt to other UNIX-like systems.

Each time the script is invoked it creates a new complete copy of a collection of source directories (the backups source) in a destination directory (the backups store). The result is what we call a snapshot. The script is typically invoked by `cron` or `anacron` to periodically create a snapshot of the backups source. It then allows to recover accidentally deleted or overwritten files and also to go back in time to explore the successive versions of a file.

The script uses a now well known and documented `rsync` trick to save disk space: instead of really creating a complete copy of the backups source in each new snapshot it always uses the most recent existing snapshot as a reference and hard links the unmodified files in the new snapshot.

The script simply adds to `rsync`:

* configurable sanity checks before new snapshots creation (disk space, age of most recent snapshot, ...),
* snapshots creation,
* detect non-normally terminated snapshots and orphan log files and remove them,
* remove snapshots after a configurable delay,
* debugging,
* logging,
* e-mail notification.

# <a name="Usage"></a>Usage

In the following we consider that a string shell variable is `SET` if it is defined and its value is anything else than the empty string. Else we say that it is `UNSET`. We consider that a numeric shell variable is `SET` if it is defined and its value is anything else than 0. Else we say that it is `UNSET`.

To launch a backup, simply invoke the script with one of the following forms:

    backups.sh
    backups.sh /usr/local/etc/backups.conf
    backups.sh file1 file2... fileN

The first and second forms are equivalent, they use the `/usr/local/etc/backups.conf` default configuration file. The third form allows to use other configuration files than the default and / or more than one configuration file. The configuration files are processed in the specified order. They contain `BACKUPS_XXX=YYY` shell variable definitions that influence the behaviour. The script sources the first configuration file and calls the `backup` shell function that actually performs the backup. It then sources the second file, calls the `backup` shell function again, and so on until the last configuration file is processed. There are as many backup operations as configuration files. For safety reasons, if one of the specified configuration file (or the default in the two first invocation forms) is not found or not readable, the script aborts with an error message.

The two most important shell variables defined in configuration files are:

* `BACKUPS_SOURCE` that lists the source directories to backup, e.g. `BACKUPS_SOURCE=/home/john /home/mary /etc`,
* `BACKUPS_STORE` that points to the storage facility where the backups are stored, e.g. `BACKUPS_STORE=doe@store.storage.org:/backups/doe`.

For each backup operation a snapshot directory and a log file are created:

    $BACKUPS_STORE/snapshots/YYYYmmddHHMMSS/
    $BACKUPS_STORE/logs/YYYYmmddHHMMSS.bz2

where `YYYYmmddHHMMSS` is the current date and time in the `strftime` format `%Y%m%d%H%M%S`. The snapshot directory contains the copy of all directories specified in `$BACKUPS_SOURCE`. The log file is a compressed output of all executed commands during the backup, including the output of `rsync`. It is created at the end of the backup, unless the backup crashed or aborted. Important: log files are used by the script to identify incomplete aborted snapshots that are marked for deletion and that are not used as a reference for the hard links in other snapshots. Log files are thus important and they should not be deleted, unless the corresponding snapshot is also deleted.

The provided `backups.conf` file is a commented example configuration file. Configuration variable definitions are persistent: the ones that are common to all configuration files do not have to be repeated in each file, as long as they are given in the first. This is convenient but must be handled with care, as it is very easy to forget to re-define or unset a variable and thus let it have an unwanted impact on the next backup operation.

Additionally, filter rules files can be used to include / exclude files and directories from the backup process: if the `BACKUPS_FILTER` configuration variable is defined in a configuration file, its value is considered as the name of a filter rules file to use. The script also comes with a commented example of filter rules file: `backups.filter`.

The first thing to do before using the script is to adapt the backup strategy to your own needs. This is done by customizing your own copy of the configuration and filter rules files. The proposed default are just examples and there is very little chance that it is what you want (no chance at all, in fact). In order to customize these files, a good understanding of `rsync` is required. Please look at the `rsync` documentation before using this script.

# <a name="Installation"></a>Installation and configuration

* Copy the `backups.sh`, `backups.conf` and `backups.filter` in appropriate locations on the computer that will run the script. Directories like `/usr/local/bin` for `backups.sh` and `/usr/local/etc` for the two others are reasonable choices. You can rename them, if you wish:
  * `backups.sh` is the script you will call at the end, so you can give it any name,
  * `/usr/local/etc/backups.conf` is the default configuration file that is used if the script is called without argument but if you do supply one or several arguments, they are considered as configuration files, so you can give it any name and put it anywhere, 
  * the `BACKUPS_FILTER` variable in the configuration file defines the filter rules file, so you can give it any name and put it anywhere.
* Edit the configuration file and adapt the variables definitions. The role of each variable is described as comments in the provided `backups.conf` example configuration file.
* If needed, edit the filter rules file and populate it with your own `rsync` filter rules. The proposed and commented rules in the provided `backups.filter` example file are just... examples.
* Configure a `cron` or an `anacron` job to run the script periodically. You can call the script with no argument or with one single argument. If it is called with no argument it will use the `/usr/local/etc/backups.conf` default configuration file. If it is called with one argument it must be the path of the configuration file. This allows you to call it several times in several `cron` or `anacron` jobs with different configuration file.

# <a name="Notes"></a> Important notes

## <a name="Safety"></a> Safety

In normal mode the script runs as root and deletes things like, for instance, temporary files. It can also overwrite files and complete directory trees. It is thus extremely important that it is tested before it is actually used.

PLEASE REMEMBER THAT ANY ERROR IN YOUR CONFIGURATION CAN LEAD TO CATASTROPHIC RESULTS AS, FOR INSTANCE, MASSIVE LOSS OF DATA. YOU HAVE BEEN WARNED.

In order to address this first concern (safety), several `BACKUPS_XXX` variables defined in the configuration file can be used:

* If `BACKUPS_ROOT_ONLY` is set, only root is allowed to run the script: if it is run by any other user the script logs an error message and exits. If `BACKUPS_ROOT_ONLY` is unset, a non root user can run the script. It is warmly advised to run the script with `BACKUPS_ROOT_ONLY` unset and as a non-privileged user during the configuration phase because the consequences of configuration error are far less catastrophic. Beware, however: a non-privileged user can still be harmful, at least to her own data. Why not always running the script in this safer mode? Because in most cases only root can perform the backups you want. But of course, if your own use of the script allows it, it is much better to run it in user mode. It may be the case, for instance, if you only want to backup a user's home directory on a file system where she has read-write permissions.
* If the backups store is not mounted on the host that runs the script, it is accessed by `ssh` and `scp` and its specification in the configuration file is of the form: `BACKUPS_STORE=user@host:path-to-store`. There again, using a non-privileged user during the configuration phase is warmly recommended, for the same reasons.
* The `BACKUPS_DEBUG` variable can be used to perform dry runs with more or less side effects. If `BACKUPS_DEBUG` is unset the script behaves normally, things can be deleted or overwritten. If it is set the script performs a dry run, without side effects apart the creation of the temporary long and short log files in the temporary directory defined by the `BACKUPS_TMP_DIR` variable. Debug messages are printed and logged. Dangerous actions are replaced by debug messages containing the command that would have been executed in normal mode.
* The `BACKUPS_MAX_NUMBER_OF_SNAPSHOTS_TO_DELETE` configuration variable controls whether too old backups and non-normally terminated backups are automatically deleted by the script or listed in a deletion script that must be run manually. Keeping this variable unset is safer, at least until the script has been carefully checked.
* The `BACKUPS_MAX_NUMBER_OF_LOGS_TO_DELETE` configuration variable does the same for the log files.

During customization it is warmly advised to first run the script with `BACKUPS_ROOT_ONLY` unset and `BACKUPS_DEBUG` set, with a dummy backups source. Test first with a local backups store owned by the non-privileged user that runs the script, then test with a remote backups store owned by a non-privileged remote user. Use aggressive values for the `BACKUPS_XXX` control variables to exercise as much as possible the different features of the script until everything looks fine. Features that should definitely be tested are:

* backups store disk space threshold (variable `BACKUPS_SIZE_MAX`)
* orphan log files detection (just create dummy log files with no corresponding snapshots)
* too old snapshots detection (create dummy snapshots with appropriate dates and times and adjust the `BACKUPS_YEARLY`, `BACKUPS_MONTHLY` and `BACKUPS_DAILY` variables)

Debug messages will be printed instead of the dangerous actions, telling you what would have been deleted or overwritten. Please carefully check these messages. As no snapshots are created, this mode is not sufficient to fully test your configuration. Once you are satisfied with this mode, unset `BACKUPS_DEBUG` and do some more tests, still as non root user(s). Everything shall run normally, including the actual creation of the snapshots. Finally, set `BACKUPS_ROOT_ONLY`, run the script as root on real backups source and backups store and cross your fingers.

## <a name="Security"></a>Security

A backup system poses many security problems that this script does not handle at all. For instance, if you backup the home directories of your users and one of them used to store sensitive passwords in a non-encrypted text file, the file can be kept for years in the backups, long time after the user realized how dangerous this was and wisely decided to encrypt the text file...

PLEASE REMEMBER THAT BACKUPS SYSTEMS CAN LEAD TO CATASTROPHIC RESULTS, AS, FOR INSTANCE, DISCLOSURE OF EXTREMELY SENSITIVE DATA. YOU HAVE BEEN WARNED.

In order to address this second issue (security), please read good computer and network security tutorials and be careful. And remember that, during backups, data are stored somewhere but also transported from a backups source to a backups store. The storage AND the transport can be security weaknesses.

## <a name="MacOSX"></a>Extra recommandations for Mac OS X users

This script uses some recent `bash` features (like the associative arrays and the `-A` option of the `local` builtin). Please check that your `bash` version is recent enough to support these features. It is thus advised to run first in debug mode and check errors and warnings. If your `bash` version is too old, please install `macport` or any equivalent and a recent version of `bash`. Then, replace the first line of the `backups.sh` script by `#!/<macportpath>/bash` where `<macportpath>` is the path of the `macport` binaries (`/opt/local/bin` by default). You should probably also consider installing the `macport` `coreutils` package that provides recent versions of most utilities, and put `/opt/local/libexec/gnubin` (or the equivalent if your `macport` installation is not the default) on front of the `PATH` variable definition in the `/usr/local/etc/backups.conf` configuration file to force their use instead of the Mac OS X defaults.

# <a name="How"></a>How does it work?

The very first things the script does is to record the current date and time in the `strftime` format `%Y%m%d%H%M%S` (`YYYYMMDDHHMMSS`). In the following we name this date and time label `CURRENT`. It is used to name the newly created snapshots, log files and cleaning scripts, as explained below. The passed arguments (default `/usr/local/etc/backups.conf`) are a list of configuration files to process. For each configuration file, in the specified order, the same process is applied (see below). Important note: variable definitions, if not overwritten, are preserved from one configuration file to the next. So, common definitions can be put in the first configuration file, there is no need to repeat them in the following files, if any. Multiple files are typically used when several different sets of `BACKUPS_SOURCE` are backed up in as many different `BACKUPS_STORE`: there is one configuration file per `BACKUPS_STORE` / `BACKUPS_SOURCE` pair but the common definitions can go in the first one only.

## <a name="EachConf"></a>Processing of a configuration file

If the configuration file is not found or not readable an error message is logged and the script exits, else the file is sourced.

The backups source must be mounted on the local host (the one that runs the script). The backups store can be:

* on a local file system or on a network file system (NFS, Samba, ...): `BACKUPS_STORE=some-path`, or
* remote and accessed only through `ssh` and `scp`: `BACKUPS_STORE=user@host:some-path`

If the `BACKUPS_STORE` variable contains a colon (`:`), the script will consider the backups store as remote and will use only `ssh` and `scp` to access it. In this case, `some-path` is converted into an absolute path by `cd some-path; pwd -P`. As this is not very robust it is strongly advised to use absolute paths. On a pure performance point of view, a local file system is slightly better than `ssh` which is far better than a network file system. So, if the backups store is not local and both methods are available, always prefer `ssh` over the network file system. My own experiments show that the same full backup that takes about 15 minutes with `ssh` takes more than 4 hours with NFS... Of course, if you use the `ssh` method, you must find a way to provide the credentials of the remote user on the remote host. Please read the `ssh` documentation to select the most appropriate way.

The newly created snapshots are stored in `$BACKUPS_STORE/snapshots` and named `$CURRENT`. In order to save disk space, files that did not change since the last snapshot are hard linked in the new one. A snapshot thus looks like a full backup while it is in fact an incremental one. For each newly created snapshot a `bzip2`-compressed detailed log file named `$CURRENT.bz2` is stored in `$BACKUPS_STORE/logs`.

The script first tests the following conditions:

* `BACKUPS_ROOT_ONLY` is unset  or the script is run as root.
* Another backup is not already running.

If any of the above conditions does not hold a message is recorded in the log and the script exits. Else, the already existing snapshots are parsed from the older to the most recent and some of them may be marked as removable:

* Non normally terminated snapshots, that is, snapshots for which there is no corresponding log file in `$BACKUPS_STORE/logs` are marked.
* If `BACKUPS_YEARLY` is set and not 0, the first snapshot of any year is marked if it is older than `$BACKUPS_YEARLY` days.
* If `BACKUPS_MONTHLY` is set and not 0, the first snapshot of any month not also the first of a year is marked if it is older than `$BACKUPS_MONTHLY` days.
* If `BACKUPS_DAILY` is set and not 0, any snapshot that is not the first of a month is marked if it is older than `$BACKUPS_DAILY` days.

For each marked snapshot a message is logged. For the expired snapshots the long log file is also marked. Orphan log files, that is, log files with no corresponding snapshot, are also marked. If the number of marked snapshots is less or equal `$BACKUPS_MAX_NUMBER_OF_SNAPSHOTS_TO_DELETE` they are deleted. Else, a cleaning script file named `$CURRENT.clean.sh` is created in the backups store. The script file contains one `rm` command per marked snapshot. Same for marked log files with the `BACKUPS_MAX_NUMBER_OF_LOGS_TO_DELETE` control variable. There are at least two good reasons to leave the `BACKUPS_MAX_NUMBER_OF_SNAPSHOTS_TO_DELETE` and `BACKUPS_MAX_NUMBER_OF_LOGS_TO_DELETE` configuration variables unset (at least until the script proved that it works as expected over a reasonably long time of use):

* Automatic deletion is too dangerous and a human shall check this before.
* If the backups store is served by NFS and the backups source is huge, removing snapshots through NFS from the local host can be very slow. Much slower than from the NFS server itself.

If the safe way is chosen, after careful verification, the script file can be run, either locally or on the remote server:

```
cd $BACKUPS_STORE
sh ./$CURRENT.clean.sh
```

While safer, there is a drawback to this delayed removal mechanism: when testing the used disk space on backups store (see below), the script will account for the marked snapshots. It could thus be that it cancels the current snapshot because the used disk space threshold is exceeded while it would not if the marked snapshots were removed first. But the safety of this solution is probably worth the drawback.

After marking the expired or non normally terminated snapshots, and deleting them if their number does not exceed the limit, the script tests some more conditions:

* `BACKUPS_SIZE_MAX` is unset or 0 (the default) or the used size (`df`) of the partition of backups store is less than `$BACKUPS_SIZE_MAX` GB.
* The most recent already existing snapshot is older than `$BACKUPS_MIN_INTERVAL` seconds, unless `BACKUPS_MIN_INTERVAL` is unset or 0 (the default).

The second condition is not tested if there are no already existing snapshots. If any of the above tested conditions does not hold, a message is logged and the script exits. Else, the backup is launched. The `BACKUPS_SOURCE` variable is mandatory and cannot be unset. It defines the directories to backup. The `BACKUPS_OPTIONS` variable defines extra options to pass to `rsync`. If `BACKUPS_FILTER` is set the `--filter='. $BACKUPS_FILTER'` option is appended to `BACKUPS_OPTIONS`. It there are already existing and not marked snapshots the `--link-dest=<newest>` option is appended to `BACKUPS_OPTIONS` where `<newest>` is the most recent of them. Finally `rsync` is called with:

```
rsync $BACKUPS_OPTIONS $BACKUPS_SOURCE $BACKUPS_STORE/snapshots/$CURRENT
```

After each invocation, a short log is appended to the common log file `$BACKUPS_LOG_FILE` and also sent by e-mail to `$BACKUPS_EMAIL` if this variable is set. If the backup fails for any reason (error, disk space, last snapshot recent enough, ...) the detailed log file is not stored in `$BACKUPS_STORE/logs`. This absence of a log file is used during subsequent invocations of the script to detect aborted backups. The detailed log file is kept in `$BACKUPS_TMP_DIR` instead. It can thus be analysed in order to understand the reasons of the failure and must be deleted manually if needed.

# <a name="Example"></a>Example of use

```
BACKUPS_OPTIONS="--verbose --archive --relative --one-file-system --cvs-exclude"
BACKUPS_SOURCE="/home /etc /usr/local"
BACKUPS_FILTER=/usr/local/etc/backups.filter
```

with the following /usr/local/etc/backups.filter file:

```
:n .backups
- .mozilla/firefox/*/Cache/
- .local/share/Trash/
- .cache/
- nosave/
- *.o
```

will recursively backup `/home`, `/etc` and `/usr/local` as `$BACKUPS_STORE/snapshots/$CURRENT/home`, `$BACKUPS_STORE/snapshots/$CURRENT/etc` and `$BACKUPS_STORE/snapshots/$CURRENT/usr/local`. The file systems boundaries will not be crossed. In each encountered directory where a file named `.backups` is found it will be parsed for per-directory `rsync` filter rules (the `n` flag in the first line of the filter rules file indicates that the per-directory rules apply to the containing directory only and are not recursively applied to the sub-directories). Moreover, any directory named `Cache` in a sub-directory of any directory named `.mozilla/firefox` will be ignored. Any directory named `.local/share/Trash`, `.cache` or `nosave` will be ignored. Any file or directory named `<something>.o` will be ignored. Please see `rsync` documentation for more detailed explanations about `rsync` options, filter rules and the specification of backups source.

# <a name="Filter"></a>Filter rules

There are several ways to define what should be backed up and what should not:

* The `BACKUPS_SOURCE` variable. If you use the `--one-file-system` `rsync` option, as suggested above, please remember that two directories from two different file systems must be explicitly listed. For example, if `/` and `/boot` are two different partitions, listing only `/` will not backup `/boot`.

* The filter rules file shall mainly be used to express include/exclude patterns that apply to your whole system. Note that the filter rules can almost completely replace the `BACKUPS_SOURCE` variable. If, as in our example, `/home`, `/etc` and `/usr/local` must be backed up, one can also set `BACKUPS_SOURCE` to `/` and use the following filter rules file:

```
+ /home
+ /etc
+ /usr
+ /usr/local
- /usr/*
- /*
```

Note that the `+ /usr` is required because if it is missing the `- /*` completely excludes `/usr` and the `+ /usr/local` has no effect at all. Note also that the `- /usr/*` is also required to exclude everything in `/usr` but `/usr/local`.

* On a multi-user system it is frequently necessary to allow individual users to decide what to exclude and what to include in the backups. The `- nosave/` rule of our example is a simple way to do that. Any directory named `nosave` will be excluded. But asking the users to put every data they want to exclude in a directory named `nosave` is not very convenient. The `:n .backups` filter rule of our example solves this by stating that any file named `.backups` will be parsed for per-directory `rsync` filter rules. Examples:
  * In order to completely exclude the content of directory `foo/` from the backup, one can create a `foo/.backups` file containing the `rsync` filter rule `- /*`:
```
$ echo '- /*' > foo/.backups
```
   Any file or sub-directory of directory `foo/` is excluded. It is thus useless to put a `.backups` file in a sub-directory `foo/bar/` of directory `foo/` because as `/foo/bar/` is excluded by the `foo/.backups` rules file, it will not even be parsed for `.backups` rules files. `foo/` is backed up as an empty directory. To completely exclude `foo/` (and not only its content), a `.backups` file in its parent directory is required:
```
$ echo '- /foo' > foo/../.backups
```
  * In order to exclude the files contained in directory `foo/` but not its sub-directories, one can create a `foo/.backups` file containing the `rsync` filter rules `+ /*/` and `- /*`:
```
$ echo '+ /*/' > foo/.backups
$ echo '- /*' >> foo/.backups
```
   The first rule adds all `foo/` sub-directories and the second excludes everything else (files). Any `.backups` rules file in `foo/` sub-directories is considered normally. 
  * If `foo/.backups` contains:
```
- /*.o
```
   then, any file or sub-directory of `foo/` named `<something>.o` is excluded. Everything else is backed up normally. The rule applies to `foo/` only, not to its sub-directories: `foo/bar/goo.o` is backed up normally.

# <a name="Recovering"></a>Recovering files and directories

Recovering files and directories from the backups store is extremely simple. Depending on your setup it can be done either by any user or only by a privileged one and it can be done through `ssh` only or by direct access in the locally mounted backups store partition. The script applies the following access permissions to the snapshots, log and cleaning files:

* `$BACKUPS_STORE`:			`u+rwx,go+rx-w`
* `$BACKUPS_STORE/snapshots`:		`u+rwx,go+rx-w`
* `$BACKUPS_STORE/snapshots/$CURRENT`:	`u+rwx,go+rx-w`
* `$BACKUPS_STORE/logs`:		`u+rwx,go+rx-w`
* `$BACKUPS_STORE/logs/$CURRENT.bz2`:	`u+rw,go-rwx`
* `$BACKUPS_STORE/$CURRENT.clean.sh`:	`u+rw,go-rwx`

`rsync` does the rest in the snapshots. So, by tuning the `BACKUPS_OPTIONS` configuration variable you can decide what non privileged users are allowed to see. My own, favourite configuration is the following: the backups store resides on a file system on a remote server. The server exports the backups store partition by NFS, read-only and to the local host only. The local host automounts the backups store with the `ro,auto,noexec,nosuid,nodev,user` options. But it uses the much faster `ssh` method for the backups. The `rsync` options are such that the owners, groups, permissions are preserved (`--archive`). If `/backups` is the NFS mount point of backups store on the local host, any user can search an accidentally deleted file or directory and get a complete list of available copies with dates and times:

```
ls -l /backups/snapshots/*/home/mary/some-path/deleted-file
-rw------- 1 mary mary 12527 Aug 21  2012 /backups/snapshots/20120822070001/home/mary/some-path/deleted-file
-rw------- 1 mary mary  8804 Aug 27  2012 /backups/snapshots/20120829070001/home/mary/some-path/deleted-file
-rw------- 1 mary mary  8695 Aug 29  2012 /backups/snapshots/20120830070001/home/mary/some-path/deleted-file
...
-rw------- 1 mary mary  9811 Aug 22 21:26 /backups/snapshots/20130823070001/home/mary/some-path/deleted-file
-rw------- 1 mary mary 12324 Aug 24 08:47 /backups/snapshots/20130824100001/home/mary/some-path/deleted-file
-rw------- 1 mary mary 12602 Aug 27 17:14 /backups/snapshots/20130828150956/home/mary/some-path/deleted-file
```

She can then pick one and copy it back in place with its original owner, group, permissions,... :

```
cp -a /backups/snapshots/20130828150956/home/mary/some-path/deleted-file /home/mary/some-path/deleted-file
```

Of course, privileged users are the only ones who can access protected files. The access control is exactly the same in backups store as on the host itself. This combination is fast and user friendly. As already mentioned in the IMPORTANT NOTES section, security is another matter that should also be considered.
